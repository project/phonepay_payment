<?php

namespace Drupal\phonepay_payment\phonepe\payments\v1\models\request\builders;

use Drupal\phonepay_payment\phonepe\payments\v1\models\request\paymentInstrument\UpiIntentPaymentInstrument;

class UpiIntentInstrumentBuilder
{

    private $targetApp;

    public function targetApp($targetApp): UpiIntentInstrumentBuilder
    {
        $this->targetApp = $targetApp;
        return $this;
    }

    public function build(): UpiIntentPaymentInstrument
    {
        return new UpiIntentPaymentInstrument($this->targetApp);
    }
}