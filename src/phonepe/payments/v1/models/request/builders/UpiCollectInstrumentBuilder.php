<?php

namespace Drupal\phonepay_payment\phonepe\payments\v1\models\request\builders;

use Drupal\phonepay_payment\phonepe\payments\v1\models\request\paymentInstrument\UpiCollectPaymentInstrument;

class UpiCollectInstrumentBuilder
{

    private $vpa;

    public function vpa($vpa): UpiCollectInstrumentBuilder
    {
        $this->vpa = $vpa;
        return $this;
    }

    public function build(): UpiCollectPaymentInstrument
    {
        return new UpiCollectPaymentInstrument($this->vpa);
    }
}