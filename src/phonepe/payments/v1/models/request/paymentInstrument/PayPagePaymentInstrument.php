<?php

namespace Drupal\phonepay_payment\phonepe\payments\v1\models\request\paymentInstrument;

use Drupal\phonepay_payment\phonepe\payments\v1\constants\PaymentInstrumentConstants;
use Drupal\phonepay_payment\phonepe\payments\v1\models\request\PaymentInstrument;

class PayPagePaymentInstrument extends PaymentInstrument
{
    public function __construct()
    {
        parent::__construct(PaymentInstrumentConstants::PAY_PAGE);
    }
}