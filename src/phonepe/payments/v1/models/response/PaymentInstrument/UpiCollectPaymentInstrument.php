<?php

namespace Drupal\phonepay_payment\phonepe\payments\v1\models\response\PaymentInstrument;

use Drupal\phonepay_payment\phonepe\payments\v1\constants\PaymentInstrumentConstants;
use Drupal\phonepay_payment\phonepe\payments\v1\models\response\PaymentInstrument;

class UpiCollectPaymentInstrument extends PaymentInstrument implements \JsonSerializable
{
    public function __construct()
    {
        parent::__construct(PaymentInstrumentConstants::UPI_COLLECT);
    }
}
