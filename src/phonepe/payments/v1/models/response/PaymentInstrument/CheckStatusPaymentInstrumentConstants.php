<?php

namespace Drupal\phonepay_payment\phonepe\payments\v1\models\response\PaymentInstrument;

class CheckStatusPaymentInstrumentConstants
{
    const UPI = "UPI";
    const CARD = "CARD";
    const NETBANKING = "NETBANKING";

}