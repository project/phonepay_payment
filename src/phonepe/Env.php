<?php
namespace Drupal\phonepay_payment\phonepe;

class Env
{
    const STAGE = "STAGE";
    const UAT = "UAT";
    const PRODUCTION = "PRODUCTION";
}