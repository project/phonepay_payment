<?php

namespace Drupal\phonepay_payment\phonepe\common\models\constants;

class TransactionType
{
    const DEBIT = "DEBIT";
    const REVERSAL = "REVERSAL";
}